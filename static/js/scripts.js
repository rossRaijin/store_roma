$(document).ready(function () {

    $('.defaultCountdown').countdown({until: new Date(2018, 3-1, 23)});

    $("[title]").tooltip();


    // $('#removeCountdown').click(function() {
    //     var destroy = $(this).text() === 'Remove';
    //     $(this).text(destroy ? 'Re-attach' : 'Remove');
    //     $('#defaultCountdown').countdown(destroy ? 'destroy' : {until: newYear});
    // });

    // $('.tab-container').easytabs();

    $('.slider').bxSlider({
        auto: true,
        controls: false,
        mode: 'fade',
        randomStart: true,
        pager: false,
    });

    function heightFix() {
        $('.sidebar').css('height', $(window).height() - 125);
    };

    heightFix();

    $(window).resize(function () {
        heightFix();
    });

    $('.product-quantity').each(function () {
        var quantity_block = $(this),
            quantity_number = parseInt(quantity_block.text());

        if (quantity_number < 10) {
            quantity_block.css(
                {'background-color': '#fd6363'}
            )
        }
    });

    $('.product-discount').each(function () {
        var disc_block = $(this),
            disc_number = parseInt(disc_block.text());

        if (disc_number > 0) {
            disc_block.css(
                {'background-color': '#92FD45'}
            )
        }
    });

    $(document).on('click', '.comment-del-img', function (e) {
        e.preventDefault();
        var item_id = $(this).attr('id');
        var this_comment = $(this).parent('.comment-wrapper');
        $('#comment-del-modal a').each(function () {
           var current = $(this).attr("href").split("=");
           var href = current[0]+"="+item_id;
           $(this).attr("href", href);
           console.log(this);
           $(this).on('click', this, function (e) {
              e.preventDefault();
              url = $(this).attr('href');
              $.ajax({
                  url: url,
                  success: function () {
                      this_comment.html('');
                      $('#comment-del-modal').modal('hide');
                  }
              });
           });
        });
    });

    $('#add-comment').click(function (e) {
        e.preventDefault();
        var form = $('#add-comment-form');
        $.post(form.attr('action'), form.serialize()).done(function(data) {
            $('#comment-box').append(
                "<div class='comment-wrapper'>"+
                "<a class='comment-edit-img'"+"href='"+data.update_url+"'>"+"<img src='/static/media/work_images/icons-edit.png'>"+"</a>"+
                "<a href='#' id="+data.id+" class='comment-del-img' data-toggle='modal' data-target='#comment-del-modal'>"+
                "<img src='/static/media/work_images/icons-delete.png'>"+
                "</a>"+
                "<p class='comment-text'>"+data.text+"</p>"+
                "<span class='comment-author'>"+data.author+"</span>"+
                "<span class='comment-date'>"+data.date+"</span>"+
                "<span>"+data.id+"</span>"+
                "</div>"
            );
        })
    });

    $(document).on('click', '.category-del-img', function (e) {
        e.preventDefault();
        var cat_id = $(this).attr('id');
        var this_category = $(this).parent().parent('.category-item');
        $('#category-del-modal a').each(function () {
           var current = $(this).attr("href").split("=");
           var href = current[0]+"="+cat_id;
           $(this).attr("href", href);
           $(this).on('click', this, function (e) {
              e.preventDefault();
              url = $(this).attr('href');
              $.ajax({
                  url: url,
                  success: function () {
                      this_category.html('');
                      $('#category-del-modal').modal('hide');
                  }
              });
           });
        });
    });

    $(document).on('click', '.product-del-img', function (e) {
        e.preventDefault();
        var item_id = $(this).attr('id');
        var this_product = $(this).parent().parent('.product-item');
        $('#product-del-modal a').each(function () {
           var current = $(this).attr("href").split("=");
           var href = current[0]+"="+item_id;
           $(this).attr("href", href);
           $(this).on('click', this, function (e) {
              e.preventDefault();
              url = $(this).attr('href');
              $.ajax({
                  url: url,
                  success: function () {
                      this_product.html('');
                      $('#product-del-modal').modal('hide');
                  }
              });
           });
        });
    });

    $(document).on('click', '.user-del-img', function (e) {
        e.preventDefault();
        var user_id = $(this).attr('id');
        var this_user = $(this).parent().parent('.user-item');
        $('#user-del-modal a').each(function () {
           var current = $(this).attr("href").split("=");
           var href = current[0]+"="+user_id;
           $(this).attr("href", href);
           $(this).on('click', this, function (e) {
              e.preventDefault();
              url = $(this).attr('href');
              $.ajax({
                  url: url,
                  success: function () {
                      this_user.html('');
                      $('#user-del-modal').modal('hide');
                  }
              });
           });
        });
    });

    $(document).on('click', '.order-del-img', function (e) {
        e.preventDefault();
        var order_id = $(this).attr('id');
        var this_order = $(this).parent().parent('.order-item');
        $('#order-del-modal a').each(function () {
           var current = $(this).attr("href").split("=");
           var href = current[0]+"="+order_id;
           $(this).attr("href", href);
           $(this).on('click', this, function (e) {
              e.preventDefault();
              url = $(this).attr('href');
              $.ajax({
                  url: url,
                  success: function () {
                      this_order.html('');
                      $('#order-del-modal').modal('hide');
                  }
              });
           });
        });
    });

    /*Infinity-scroll*/
    var infinite = new Waypoint.Infinite({
                          element: $('.infinite-container')[0],
                          onBeforePageLoad: function () {
                            $('.loading').show();
                          },
                          onAfterPageLoad: function ($items) {
                            $('.loading').hide();
                          }
                        });

    /*Скрываю/показываю комментарии*/
    $(document).on('click', '.show-content', function () {
        $(this).parent().next().toggleClass('special');
        $(this).parent().next().children().find('.all-comments').toggleClass('special');
    });

    /*Скрипт добавления сообщения на стену*/
    // $('.message_form').on('submit', function (e) {
    //     e.preventDefault();
    //     var form = $(this);
    //     $.post(form.attr('action'), form.serialize()).done(function (data) { /*ajax-запрос для добавления сообщения*/
    //         $('.message-container').prepend( /*Новые сообщения добавляются сверху*/
    //             "<div class='message-wrapper'>" +
    //                 "<div class='message-body'>" +
    //                 "<a class='edit-message'"+"href='"+data.update_url+"'>редактировать сообщение</a>"+
    //                 "<p class='msg-body'>" + data.body + "</p>" + "Сообщение опубликовано" +
    //                 "<span class='message-author'>" + data.author + "</span>" +
    //                 "<span class='message-date'"+"data-url='"+data.create_url+"'"+">" + data.date + "</span>" +
    //                 "<a class='reply'>комментировать</a>" + "<p class='show-content'>Показать все комментарии</p>" +
    //                 "</div>" + "<div class='all-comments special'></div>" +
    //             "</div>"
    //         );
    //     });
    //     form.find("textarea").val(''); /*Чистим форму после добавления сообщения*/
    // });

    /*Скрипт для получения формы сообщения для редактирования*/
    // $(document).on('click', '.edit-message', function(e) {
    //     e.preventDefault();
    //     // $(this).attr('onclick', '');
    //     var edited_obj = $(this).parent(),
    //         val_for_edit = edited_obj.find('p').first().text(), /*Получаю начальное значение редактируемого сообщения для вставки в форму*/
    //         action = $(this).attr('href'),
    //         clone = $('.message_form').clone().attr('action', action).css('display', 'block'); /*Клонирую форму добавления сообщения*/
    //
    //     $(this).after(clone);
    //     clone.addClass('edit_message_form').removeClass('message_form');
    //     $('.edit_message_form p textarea').val(val_for_edit);
    //     clone.attr('onclick', editmessage()); /*Привязываю к измененному(новому) объекту событие*/
    // });

    /*Функция для редактирования сообщения*/
    // function editmessage() {
    //     $('.edit_message_form').on('submit', function (e) {
    //         e.preventDefault();
    //         var form = $(this);
    //         $.post(form.attr('action'), form.serialize()).done(function (data) {
    //             form.parent().replaceWith(/*Заменяю редактриуемые данные на новые*/
    //                 "<div class='message-body'>" +
    //                 "<a class='edit-message'"+"href='"+data.update_url+"'>редактировать сообщение</a>"+
    //                 "<p class='msg-body'>" + data.body + "</p>" + "Сообщение отредактировано" +
    //                 "<span class='message-author'>" + data.author + "</span>" +
    //                 "<span class='message-date'"+"data-url='"+data.create_url+"'"+">" + data.date + "</span>" +
    //                 "<a class='reply'>комментировать</a>" + "<p class='show-content'>Показать все комментарии</p>" +
    //                 "</div>"
    //             );
    //             $('.void-class').remove();
    //             form.remove();/*Удаляю форму*/
    //         });
    //     });
    // }

    /*Скрипт для клонирования формы для добавления комментария*/
    $(document).on('click','.comment, .reply', function(e) {
        var $action = $(this).prev(),
            action = $action.attr('data-url'),
            clone = $('.comment_form').clone().attr('action', action).css('display', 'block');

        $(this).after(clone);
        clone.addClass('add_comment_form').removeClass('comment_form');
        clone.attr('onclick', comment());
    });

    /*Функция для добавления комментария*/
    function comment() {
        $('.add_comment_form').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            $.post(form.attr('action'), form.serialize()).done(function (data) {
                form.parent().next().append(
                    "<div class='comment_div'>" +
                    "<div class='comment-wrapper'>" +
                    "<a class='edit-comment'"+"href='"+data.update_url+"'>редактировать комментарий</a>"+
                    "<p class='comment-text'>" + data.body + "</p>" + "Комментарий оставлен" +
                    "<span class='comment-author'>" + data.author + "</span>" +
                    "<span class='comment-date'"+"data-url='"+data.create_url+"'"+">" + data.date + "</span>" +
                    "<a class='reply'>комментировать</a>"+
                    "</div>" + "<div class='all-comments special'>" + "</div>" +
                    "</div>"
                ).addClass('special');
                $('.void-class').remove();
                form.remove();/*Удаляю форму*/
            });
        });
    }

    /*Скрипт для получения формы комментария для редактирования*/
    $(document).on('click', '.edit-comment', function(e) {
        e.preventDefault();
        var edited_obj = $(this).parent(),
            val_for_edit = edited_obj.find('p').first().text(),/*Получаю начальное значение редактируемого сообщения для вставки в форму*/
            action = $(this).attr('href'),
            clone = $('.comment_form').clone().attr('action', action).css('display', 'block');/*Клонирую форму добавления комментария*/

        $(this).after(clone);
        clone.addClass('add_comment_form').removeClass('comment_form');
        $('.add_comment_form p input').val(val_for_edit);
        clone.attr('onclick', editcomment()); /*Привязываю к измененному(новому) объекту событие*/
    });

    /*Функция для редактирования комментария*/
    function editcomment() {
        $('.add_comment_form').on('submit', function (e) {
            e.preventDefault();
            var form = $(this);
            $.post(form.attr('action'), form.serialize()).done(function (data) {
                form.parent().replaceWith( /*Заменяю редактриуемые данные на новые*/
                    "<div class='comment-wrapper'>" +
                    "<a class='edit-comment'"+"href='"+data.update_url+"'>редактировать комментарий</a>"+
                    "<p class='comment-text'>" + data.body + "</p>" + "Комментарий отредактирован" +
                    "<span class='comment-author'>" + data.author + "</span>" +
                    "<span class='comment-date'"+"data-url='"+data.create_url+"'"+">" + data.date + "</span>" +
                    "<a class='reply'>комментировать</a>"+
                    "</div>"
                ).addClass('special');
                $('.void-class').remove();
                form.remove();/*Удаляю форму*/
            });
        });
    }

});