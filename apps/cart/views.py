from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.views.decorators.http import require_POST
from apps.cart.cart import Cart
from apps.cart.forms import CartAddProductForm
from apps.product.models import Item
from apps.coupons.forms import CouponApplyForm


@require_POST
def cart_add(request, item_id):
    cart = Cart(request)
    item = get_object_or_404(Item, id=item_id)
    form = CartAddProductForm(request.POST)
    x = request.POST.get('quantity')
    if x == None: x = 1
    y = item.quantity
    if form.is_valid() and y >= int(x) :
        cart.add(item=item, quantity=form.cleaned_data['quantity'], update_quantity=form.cleaned_data['update'])
    return redirect(reverse('product:details', kwargs={'slug':item.slug}))


def cart_remove(request, item_id):
    cart = Cart(request)
    item = get_object_or_404(Item, id=item_id)
    cart.remove(item)
    return redirect('cart:cart_detail')


def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(initial={'quantity': item['quantity'], 'update': True})
    coupon_apply_form = CouponApplyForm()

    return render(request, 'cart/cart-detail.html', {'cart': cart,
                                                     'coupon_apply_form': coupon_apply_form })
