from apps.cart.cart import Cart


#  makes the cart available anywhere in the document
def cart(request):
    return {'cart': Cart(request)}
