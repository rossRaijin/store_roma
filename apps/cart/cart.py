from decimal import Decimal
from django.conf import settings
from apps.product.models import Item
from apps.coupons.models import Coupon


class Cart:
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart
        self.coupon_id = self.session.get('coupon_id')

    def add(self, item, quantity=1, update_quantity=False):
        """
        Add item in the cart or update his quantity
        """
        item_id = str(item.id)
        if item.discount > 0 :
            price_in_cart = item.get_price_with_discount()
        else:
            price_in_cart = item.price

        if item_id not in self.cart:
            self.cart[item_id] = {'quantity': 1, 'price': str(price_in_cart)}

        if update_quantity:
            self.cart[item_id]['quantity'] = quantity

        self.save()

    def save(self):
        #update the session cart
        self.session[settings.CART_SESSION_ID] = self.cart
        #mark the session as "modified" to make sure it is saved
        self.session.modified = True

    def remove(self, item):
        item_id = str(item.id)
        if item_id in self.cart:
            item.quantity += self.cart[item_id]['quantity']
            item.save()
            del self.cart[item_id]
            self.save()

    def __iter__(self):
        item_ids = self.cart.keys()
        items = Item.objects.filter(id__in=item_ids)

        for item in items:
            self.cart[str(item.id)]['item'] = item

        for item in self.cart.values():
            item['price'] = item['price']
            item['total_price'] = item['price']*item['quantity']
            yield item

    def __len__(self):
        return sum(item['quantity'] for item in self.cart.values())

    def get_total_price(self):
        return sum(Decimal(item['price'])*item['quantity'] for item in self.cart.values())

    def clear(self):
        self.session[settings.CART_SESSION_ID] = {}
        self.session['coupon_id'] = None
        self.session.modified = True

    @property
    def coupon(self):
        if self.coupon_id:
            return Coupon.objects.get(id=self.coupon_id)
        return None

    def get_discount(self):
        if self.coupon:
            return (self.coupon.discount / Decimal('100'))*self.get_total_price()
        return Decimal('0')

    def get_total_price_after_discount(self):
        return self.get_total_price() - self.get_discount()
