from django.conf.urls import url
from apps.account.views import *


urlpatterns = [
    url(r'^(?P<id>\d+)$', ProfilePage.as_view(), name="page"),
    # url(r'^product/(?P<slug>[\w-]+)/$', ItemDetail.as_view(), name="details"),
    # url(r'^(?P<slug>[\w-]+)/$', GeneralCategory.as_view(), name="category"),
    # url(r'^$', MainPage.as_view(), name="main_page"),
]