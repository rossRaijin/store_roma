from django.http import Http404
from django.views.generic import DetailView

from apps.account.models import Profile
from apps.orders.models import Order


class ProfilePage(DetailView):
    model = Profile
    template_name = 'accounts/profile-page.html'
    pk_url_kwarg = 'id'
    context_object_name = 'profile'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        #  Костыль на покупателя
        context['orders'] = Order.objects.filter(email=self.object.user.email).prefetch_related('items')
        return context

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            self.object = self.get_object()
            if self.object.user == request.user or request.user.is_staff:
                return super().get(request, *args, **kwargs)
            else:
                raise Http404