from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^comment/(?P<id>\d+)/(?P<pk>\d+)/$', AddComment.as_view(), name='add-comment'),
    url(r'^comment/edit/(?P<id>\d+)/$', EditComment.as_view(), name='edit-comment'),
]