from django.http import Http404, JsonResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, DeleteView

from apps.product.models import Item
from apps.wall.models import Comment


class AddComment(CreateView):
    model = Comment
    fields = ['body']
    template_name = 'wall/wall-msg.html'
    # success_url = 'wall:msg-wall'

# Разрешение комментировать только для аутентифицированных пользователей
    def post(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super().post(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.author = self.request.user
        try:  # Получить родительский комментарий или это нулевой коммент
            self.object.parent = Comment.objects.get(pk=self.kwargs['pk'])
        except:
            self.object.parent = None
        self.object.product = Item.objects.get(id=self.kwargs['id'])
        self.object.save()
        if self.request.is_ajax():  # Передача словаря в ajax
            data = {
                'body': self.object.body,
                'author': self.object.author.username,
                'product': self.object.product.id,
                'date': self.object.posted.strftime('%Y-%m-%d %H:%M'),
                'update_url': self.object.get_update_url(),
                'create_url': self.object.get_create_url(),
            }
            return JsonResponse(data)
        return reverse('product:details', kwargs={'slug': self.object.product.slug})

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def get_success_url(self):
        return reverse('product:details', kwargs={'slug': self.object.product.slug})


class EditComment(UpdateView):
    model = Comment
    fields = ['body']
    template_name = 'wall/wall-msg.html'
    pk_url_kwarg = 'id'
    # success_url = '/wall/'

# Разрешение на получение формы редактироавния только для автора комментария и staff
    def get(self, request, *args, **kwargs):
        obj = Comment.objects.get(id=self.kwargs['id'])
        if request.user == obj.author or request.user.is_staff:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

# Разрешение на редактироавние только для автора комментария и staff
    def post(self, request, *args, **kwargs):
        obj = Comment.objects.get(id=self.kwargs['id'])
        if request.user == obj.author or request.user.is_staff:
            return super().post(request, *args, **kwargs)
        else:
            raise Http404

    def form_valid(self, form):
        response = super().form_valid(form)
        if self.request.is_ajax():  # Передача словаря в ajax
            data = {
                'body': self.object.body,
                'author': self.object.author.username,
                'product': self.object.product.id,
                'date': self.object.edited.strftime('%Y-%m-%d %H:%M'),
                'update_url': self.object.get_update_url(),
                'create_url': self.object.get_create_url(),
            }
            return JsonResponse(data)
        else:
            return response

    def form_invalid(self, form):
        response = super().form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)
        else:
            return response

    def get_success_url(self):
        return reverse('product:details', kwargs={'slug': self.object.product.slug})


"""Разрешение на удаление только для staff"""


class CommentDelete(DeleteView):
    model = Comment

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_staff:
            self.object.delete()
            return redirect('wall:msg-wall')
        else:
            raise Http404
