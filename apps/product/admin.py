from django.contrib import admin
from .models import Category, Item, ViewedItem


class CategoryAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Category._meta.fields]
    prepopulated_fields = {'slug':('title',)}

    class Meta:
        model: Category

admin.site.register(Category, CategoryAdmin)


class ItemAdmin(admin.ModelAdmin):
    list_display = [field.name for field in Item._meta.fields]
    list_editable = ['price', 'quantity', 'available']
    prepopulated_fields = {'slug':('item_title',)}

    class Meta:
        model: Item

admin.site.register(Item, ItemAdmin)


class ViewedItemAdmin(admin.ModelAdmin):
    list_display = ['user', 'product', 'counter']

    class Meta:
        model: ViewedItem

admin.site.register(ViewedItem, ViewedItemAdmin)