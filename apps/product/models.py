from decimal import Decimal
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import pre_save
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from apps.coupons.models import Coupon


class Category(models.Model):
    title = models.CharField(max_length=50, unique=True, default=None, verbose_name=_('Title'))
    title_ru = models.CharField(max_length=50, unique=True, blank=True, null=True, default=None)
    slug = models.SlugField(unique=True, default=None)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children', verbose_name=_('Parent'))
    created = models.DateField(auto_now_add=True, blank=True, null=True)
    cat_image = models.ImageField(upload_to="catbg", null=True, verbose_name=_('Cat_bg'))

    def __str__(self):
        return self.slug

    def get_absolute_url(self):
        return reverse("product:category", kwargs={"slug": self.slug})

    def get_api_url(self):
        return reverse("product-api:category-detail", kwargs={"pk": self.pk})

    def items_in_category_count(self):
        return Item.objects.filter(category__slug=self.slug).count()

    def first_item_in_cat(self):
        return Item.objects.filter(category__slug=self.slug).first()


def create_category_slug(instance, new_slug=None):
    slug = slugify(instance.title)
    if new_slug is not None:
        slug = new_slug
    qs = Category.objects.filter(slug=slug)
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_category_slug(instance, new_slug=new_slug)
    return slug

def pre_save_category_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_category_slug(instance)

pre_save.connect(pre_save_category_post_receiver, sender=Category)


class Item(models.Model):
    item_title = models.CharField(max_length=100, unique=True, default=None, verbose_name=_('Item title'))
    item_title_ru = models.CharField(max_length=100, unique=True, blank=True, null=True, verbose_name=_('Item title'))
    slug = models.SlugField(unique=True, default=None)
    description = models.TextField(blank=True, null=True, default=None, verbose_name=_('Description'))
    # description_ru = models.TextField(blank=True, null=True, default=None, verbose_name=_('Description_ru'))
    quantity = models.PositiveIntegerField(verbose_name=_('Quantity')) #need replace quantity at stock
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_('Price'))
    image = models.ImageField(upload_to="item_images", null=True, verbose_name=_('Image'))
    category = models.ManyToManyField(Category, related_name='items', verbose_name=_('Category'))
    views = models.IntegerField(default=0, verbose_name=_('Views'))
    available = models.BooleanField(default=False, verbose_name=_('Avaliable'))
    discount = models.PositiveIntegerField( null=True, blank=True, default=0, verbose_name=_('Discount'))
    created_date = models.DateTimeField(default=timezone.now, verbose_name=_('Added'))
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False, verbose_name=_('Changed'))

    class Meta:
        ordering = ["-created_date"]

    def __str__(self):
        return self.item_title

    def update_counter(self, user):
        #  If user is anonymous, don't update counter, becouse can't add item to user viewed items
        if user.is_authenticated:
            viewed_item, created = ViewedItem.objects.get_or_create(user=user, product=self)
            viewed_item.update_counter()

    def get_price_with_discount(self):
        return self.price - self.price*(Decimal(self.discount)/100)

    def get_absolute_url(self):
        return reverse("product:details", kwargs={"slug": self.slug})

    def get_api_url(self):
        return reverse("product-api:detail", kwargs={"slug": self.slug})


def create_item_slug(instance, new_slug=None):
    slug = slugify(instance.item_title)
    if new_slug is not None:
        slug = new_slug
    qs = Item.objects.filter(slug=slug).order_by("-id")
    exists = qs.exists()
    if exists:
        new_slug = "%s-%s" %(slug, qs.first().id)
        return create_item_slug(instance, new_slug=new_slug)
    return slug

def pre_save_item_post_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = create_item_slug(instance)

pre_save.connect(pre_save_item_post_receiver, sender=Item)


class ViewedItem(models.Model):
    user = models.ForeignKey(User, verbose_name=_('user'))
    product = models.ForeignKey(Item, on_delete=models.CASCADE, verbose_name=_('product'))
    date = models.DateField(auto_now=True)
    counter = models.IntegerField(default=1)

    def __str__(self):
        return self.product.item_title

    def update_counter(self):
        self.counter += 1
        self.save()
        return self

    class Meta:
        verbose_name = _('ViewedItem')
        verbose_name_plural = _('ViewedItems')