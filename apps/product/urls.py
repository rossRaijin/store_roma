from django.conf.urls import url
from apps.product.views import *


urlpatterns = [
    # пока тут
    url(r'^delivery/$', Delivery.as_view(), name="delivery"),
    url(r'^payments/$', PaymentMethods.as_view(), name="payments"),
    url(r'^contacts/$', Contacts.as_view(), name="contacts"),

    url(r'^sales/$', Sales.as_view(), name="sales"),
    url(r'^actions/$', Actions.as_view(), name="actions"),
    url(r'^presents/$', Presents.as_view(), name="presents"),
    url(r'^hot/$', HotProposition.as_view(), name="hot"),
    url(r'^bonuses/$', Bonuses.as_view(), name="bonuses"),
    url(r'^product/(?P<slug>[\w-]+)/$', ItemDetail.as_view(), name="details"),
    url(r'^(?P<slug>[\w-]+)/$', GeneralCategory.as_view(), name="category"),
    url(r'^$', MainPage.as_view(), name="main_page"),
]