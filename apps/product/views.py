from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.views.generic import DetailView, ListView, TemplateView
from django_filters.views import FilterView
from django.utils.translation import gettext as _

from apps.wall.forms import AddCommentForm
# from apps.wall.models import Message
from apps.wall.models import Comment
from split_testing.utils import ab_init

from apps.administrator.forms import ProductForm
from apps.cart.forms import CartAddProductForm
# from apps.comments.forms import CommentAddForm
# from apps.comments.models import Comment
from apps.product.filters import ProductFilter
from apps.product.forms import ContactForm
from .models import *


class MyMixin:
    def get_queryset(self, my_list=None):
        if not my_list:
            my_list = Item.objects.filter(category__slug=self.kwargs['slug']).order_by('item_title')
        return my_list

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['feedback'] = ContactForm()
        return context


class MainPage(ListView):
    model = Item
    queryset = Item.objects.all()
    template_name = 'main.html'
    # alternative template for a-b split-testing
    alternative_template_name = 'alter-main.html'
    page_name = 'main page'

    def dispatch(self, request, *args, **kwargs):
        # init for a/b, add +1 to "Entered"
        ab_init(self)
        # call the view
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        all_items = self.queryset  # get all products in one request
        #  rendering viewed and recommended for user
        if self.request.user.is_authenticated:
            rec_list = []
            viewed_list = ViewedItem.objects.filter(user=self.request.user).order_by('counter')[:4]
            for item in viewed_list:
                main_cat = item.product.category.all().filter(parent=None)
                new_items = all_items.filter(Q(category=main_cat) & ~Q(item_title=item.product.item_title)).order_by('?')[1]
                rec_list.append(new_items)
            context['viewed'] = viewed_list
        else:
            # top views items for anonymous
            rec_list = all_items.order_by('views')[:4]
        # rows of products by characteristics
        context['category'] = Category.objects.filter(parent=None)
        context['promotions'] = all_items.filter(discount__gt=0).order_by('discount')[:4]
        context['novelties'] = all_items.order_by('created_date')[:4]
        context['recommended'] = rec_list
        context['feedback'] = ContactForm()
        return context


class Actions(TemplateView):
    template_name = 'actions.html'


class Presents(TemplateView):
    template_name = 'presents.html'


class HotProposition(TemplateView):
    template_name = 'hot-proposition.html'


class Bonuses(TemplateView):
    template_name = 'bonuses.html'


class Delivery(TemplateView):
    template_name = 'delivery.html'


class PaymentMethods(TemplateView):
    template_name = 'payment-methods.html'


class Contacts(TemplateView):
    template_name = 'contacts.html'


class Sales(FilterView, ListView):
    model = Item
    filterset_class = ProductFilter
    template_name = 'product/product-list.html'
    context_object_name = 'items'
    queryset = Item.objects.filter(discount__gt=0).order_by('discount')
    paginate_by = 1
    page_kwarg = _('page')


class GeneralCategory(FilterView, MyMixin, ListView):
    model = Item
    filterset_class = ProductFilter
    template_name = 'product/product-list.html'
    context_object_name = 'items'
    paginate_by = 1
    page_kwarg = _('page')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['current'] = get_object_or_404(Category, slug=self.kwargs['slug'])
        context['subcategories'] = Category.objects.filter(parent__slug=self.kwargs['slug'])
        return context


class ItemDetail(MyMixin, DetailView):
    """
    Item page
    """
    model = Item
    template_name = 'product/product-details.html'
    context_object_name = 'product'

    def get_object(self, queryset=None):
        queryset = Item.objects.get(slug=self.kwargs.get('slug'))
        queryset.update_counter(self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        item_slug = self.kwargs['slug']

        if not 'visited_list' in self.request.session:
            self.request.session['visited_list'] = []

        if not item_slug in self.request.session['visited_list']:
            self.object.views += 1
            self.object.save()
            self.request.session['visited_list'].append(item_slug)

        context['addProdform'] = ProductForm  # remove in future
        context['cart_product_form'] = CartAddProductForm()
        context['comments'] = Comment.objects.filter(product__slug=self.kwargs['slug'])
        context['popular_item_list'] = Item.objects.order_by('-views')[:4]
        context['sales_item_list'] = Item.objects.filter(discount__gt=0)[:4]
        context['AddCommentForm'] = AddCommentForm()
        return context


