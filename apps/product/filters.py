import django_filters
from django import forms
from apps.product.models import Item, Category
from django.utils.translation import gettext as _


"""My task in future chaine-filtering"""


class ProductFilter(django_filters.FilterSet):
    item_title = django_filters.CharFilter(lookup_expr='icontains')
    description = django_filters.CharFilter(lookup_expr='icontains')
    category_brands = django_filters.ModelMultipleChoiceFilter(name="category",
                                                               queryset=Category.objects.filter(parent__slug="brands"),
                                                               widget=forms.CheckboxSelectMultiple)
    category_irons = django_filters.ModelMultipleChoiceFilter(name="category",
                                                              queryset=Category.objects.filter(title="Irons"),
                                                              widget=forms.CheckboxSelectMultiple)
    min_price = django_filters.NumberFilter(name="price", lookup_expr='gte')
    max_price = django_filters.NumberFilter(name="price", lookup_expr='lte')
    # date_range = django_filters.DateRangeFilter(name='updated_date')

    """Task for future"""
    # the_goods_end = django_filters

    """Ordering"""
    order_by_fields = ['ordering', 'popular']
    ordering = django_filters.OrderingFilter(fields=[['price', _('By Price')]])
    popular = django_filters.OrderingFilter(fields=[['views', _('By Populars')]])

    class Meta:
        model = Item
        fields = []