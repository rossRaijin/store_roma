from django import template

from apps.product.models import *

register = template.Library()

@register.simple_tag
def get_main_category_list():
    categories = Category.objects.filter(parent__title=None)

    return categories