from django import template
from django.utils.translation import get_language


register = template.Library()


@register.filter(name='translate_field')
def translate_field( object_to_translate, field_to_translate ):
    lang = get_language()
    try:
        if lang != 'en':
            return getattr(object_to_translate, field_to_translate +'_' + lang)
        return getattr(object_to_translate, field_to_translate)
    except AttributeError:
        pass

    return ''