# from django.contrib import auth
# from django.contrib.auth.models import User
# from django.test import TestCase, Client
#
# from apps.product.models import *
#
#
# class MainPageView(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cat = Category.objects.create(title='fish')
#         cat.save()
#         for title in ["moiva", "plotva", "botva", "iva", "moa", "moka"]:
#             product = Item.objects.create(item_title=title)
#             product.category.add(cat)
#
#     def test_products_list_url_exists(self):
#         self.assertEqual(self.client.get('/').status_code, 200)
#
#     def test_view_uses_correct_template(self):
#         resp = self.client.get('/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'main.html')
#
#     def test_view_items_list(self):
#         items_list = Item.objects.all()
#         self.assertEquals(len(items_list), 6)
#
#     def test_view_popular_list(self):
#         popular = Item.objects.order_by('-views')[:4]
#         self.assertEquals(len(popular), 4)
#
#
# class CategoryViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cat = Category.objects.create(title='fish')
#         cat.save()
#         for title in ["moiva", "plotva", "botva", "iva", "moa", "moka"]:
#             product = Item.objects.create(item_title=title)
#             product.category.add(cat)
#
#     def test_general_list_url_exists(self):
#         resp = self.client.get('/category/fish/')
#         self.assertEqual(resp.status_code, 200)
#
#     def test_general_list_uses_correct_template(self):
#         resp = self.client.get('/category/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'items/category.html')
#
#     def test_paginator_work(self):
#         resp = self.client.get('/category/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTrue('queryset' in resp.context)
#         self.assertTrue(len(resp.context['queryset']) == 4)
#         resp2 = self.client.get('/category/fish/' + '?page=2')
#         self.assertEqual(resp2.status_code, 200)
#         self.assertTrue(len(resp2.context['queryset']) == 2)
#
#
# class SubcategoryListViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cat = Category.objects.create(title='fish')
#         cat.save()
#         for title in ["moiva", "plotva", "botva", "iva", "moa", "moka"]:
#             product = Item.objects.create(item_title=title)
#             product.category.add(cat)
#
#     def test_general_list_url_exists(self):
#         self.assertEqual(self.client.get('/subcategory/fish/').status_code, 200)
#
#     def test_general_list_uses_correct_template(self):
#         resp = self.client.get('/subcategory/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'items/subcategory.html')
#
#     def test_paginator_work(self):
#         resp = self.client.get('/subcategory/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTrue('queryset' in resp.context)
#         self.assertTrue(len(resp.context['queryset']) == 4)
#
#         resp2 = self.client.get('/subcategory/fish/' + '?page=2')
#         self.assertEqual(resp2.status_code, 200)
#         self.assertTrue(len(resp2.context['queryset']) == 2)
#
#
# class ChildSubcategoryListViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         cat = Category.objects.create(title='fish')
#         cat.save()
#         for title in ["moiva", "plotva", "botva", "iva", "moa", "moka"]:
#             product = Item.objects.create(item_title=title)
#             product.category.add(cat)
#
#     def test_general_list_url_exists(self):
#         self.assertEqual(self.client.get('/childs/fish/').status_code, 200)
#
#     def test_general_list_uses_correct_template(self):
#         resp = self.client.get('/childs/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'items/child_sub.html')
#
#     def test_paginator_work(self):
#         resp = self.client.get('/childs/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTrue('queryset' in resp.context)
#         self.assertTrue(len(resp.context['queryset']) == 4)
#
#         resp2 = self.client.get('/childs/fish/' + '?page=2')
#         self.assertEqual(resp2.status_code, 200)
#         self.assertTrue(len(resp2.context['queryset']) == 2)
#
#
# class ItemDetailsViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         product = Item.objects.create(item_title="fish", slug="fish")
#
#     def test_item_details_url_exists(self):
#         self.assertEqual(self.client.get('/product/fish/').status_code, 200)
#
#     def test_item_details_uses_correct_template(self):
#         resp = self.client.get('/product/fish/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'items/product.html')
#
#     def test_item_created_slug_field(self):
#         product = Item.objects.create(item_title="plotva")
#         self.assertEquals(product.slug, 'plotva')
#
#
# class LoginViewTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         user1 = User.objects.create_user('vasya', 'vasya123')
#         user2 = User.objects.create_user('petya', 'petya123')
#
#     def test_login_url_exists(self):
#         self.assertEqual(self.client.get('/auth/login/').status_code, 200)
#
#     def test_login_view_uses_correct_template(self):
#         resp = self.client.get('/auth/login/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'main.html')
#
#     def test_count_user_list(self):
#         users = User.objects.all()
#         self.assertEqual(users.count(), 2)
#
#     def test_user_login(self):
#         user = Client()
#         resp = user.post('/auth/login/', {'username': 'Ivan', 'password': 'ivan1234'})
#         self.assertEqual(resp.status_code, 200)
#
#     def test_for_correct_user_password(self):
#         some_user = User.objects.create_user(username='vasyaa', password='vasya1234')
#         resp = some_user.check_password('vasya1234')
#         self.assertTrue(resp, True)
#
#     def test_for_redirect_when_user_login(self):
#         user = User.objects.create_user('john', 'john@mail.com', 'password')
#         username = 'john'
#         password = 'password'
#         user_ = auth.authenticate(username=username, password=password)
#         resp = self.client.post('/auth/login/', {'username':username, 'password':password})
#         self.assertRedirects( resp , '/', fetch_redirect_response=False)
#         self.assertTrue(user, user_)
#
#
# class LogoutViewChortTest(TestCase):
#
#     def test_logout_redirect(self):
#         username = 'john'
#         password = 'password'
#         resp = self.client.post('/auth/logout/', {'username':username, 'password':password})
#         self.assertRedirects(resp, '/', fetch_redirect_response=False)
#
# class RegisterViewTest(TestCase):
#
#     def test_registration_user_url_exists(self):
#         self.assertEqual(self.client.get('/auth/register/').status_code, 200)
#
#     def test_registration_uses_correct_template(self):
#         resp = self.client.get('/auth/register/')
#         self.assertEqual(resp.status_code, 200)
#         self.assertTemplateUsed(resp, 'register.html')
#
#     def test_registration_form(self):
#         pass