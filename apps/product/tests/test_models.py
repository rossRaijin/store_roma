# from django.test import TestCase
#
# from apps.product.models import *
#
#
# class CategoryModelTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         Category.objects.create(title="First")
#
#     def test_cat_title_max_length(self):
#         category = Category.objects.get(id=1)
#         max_length = category._meta.get_field('title').max_length
#         self.assertEquals(max_length, 50)
#
#     def test_get_absolute_url(self):
#         category = Category.objects.get(title="First")
#         self.assertEquals(category.get_absolute_url(), '/category/First/')
#
#
# class ItemModelTest(TestCase):
#     @classmethod
#     def setUpTestData(cls):
#         Item.objects.create(item_title="computer",
#                             description="any text here...",
#                             quantity = 20,
#                             price = 1000,
#                             created_date = timezone.now())
#
#     def test_item_title_max_length(self):
#         product = Item.objects.get(id=1)
#         max_length = product._meta.get_field("item_title").max_length
#         self.assertEquals(max_length, 100)
#
#     def test_unique_item_title(self):
#         product = Item.objects.get(id=1)
#         try:
#             Item.objects.create(item_title = "computer")
#         except Exception as ex:
#             pass
#         else:
#             self.fail("GG")
#
#     def test_item_slug_field(self):
#         product = Item.objects.get(id=1)
#         self.assertEquals(product.slug, "computer")
#
#
