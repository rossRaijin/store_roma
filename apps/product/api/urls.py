from django.conf.urls import url
from .views import *


urlpatterns = [
    url(r'^categories/$', CategoriesListAPIView.as_view(), name='category-list'),
    url(r'^categories/(?P<pk>\d+)/$', CategoryDetailAPIView.as_view(), name='category-detail'),

    url(r'^$', ProductListAPIView.as_view(), name='list'),
    url(r'^(?P<slug>[\w-]+)/$', ProductDetailAPIView.as_view(), name='detail'),
]