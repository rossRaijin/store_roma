from django.db.models import Q
from rest_framework import generics
from rest_framework.mixins import DestroyModelMixin, UpdateModelMixin
from rest_framework.permissions import IsAdminUser, AllowAny
from rest_framework.filters import SearchFilter, OrderingFilter

from apps.product.api.permissions import IsAdminOrReadOnly
from .pagination import *
from .serializers import *


class CategoriesListAPIView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoriesListSerializer
    permission_classes = [AllowAny]
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['title']

    def get_queryset(self, *args, **kwargs):
        queryset_list = super().get_queryset(*args, **kwargs)
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(title__icontains=query)
        return queryset_list


class CategoryDetailAPIView(DestroyModelMixin, UpdateModelMixin, generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategoryDetailSerializer
    permission_classes = [IsAdminUser]

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class ProductListAPIView(generics.ListAPIView):
    queryset = Item.objects.all()
    serializer_class = ProductListSerializer
    pagination_class = ProductLimitOffsetPagination
    permission_classes = [AllowAny]
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['item_title', 'description', 'user_first_name']

    def get_queryset(self, *args, **kwargs):
        queryset_list = super().get_queryset(*args, **kwargs)
        query = self.request.GET.get('q')
        if query:
            queryset_list = queryset_list.filter(Q(item_title__icontains=query) |
                                                 Q(description__icontains=query) |
                                                 Q(user_first_name__icontains=query)).distinct()
        return queryset_list


class ProductDetailAPIView(DestroyModelMixin, UpdateModelMixin, generics.RetrieveAPIView):
    queryset = Item.objects.all()
    serializer_class = ProductDetailSerializer
    permission_classes = [IsAdminOrReadOnly]
    lookup_field = 'slug'

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(user=self.request.user)

    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
