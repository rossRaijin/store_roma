from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from apps.product.models import Item, Category
# from apps.comments.api.serializers import CommentDetailSerializer
# from apps.comments.models import Comment


category_detail_url = HyperlinkedIdentityField(
    view_name='product-api:category-detail'
)


class CategoriesListSerializer(serializers.ModelSerializer):
    url = category_detail_url
    class Meta:
        model = Category
        fields = [
            'title',
            'slug',
            'id',
            'url',
        ]


class CategoryDetailSerializer(serializers.ModelSerializer):
    subcategories = serializers.SerializerMethodField()
    subcategories_count = serializers.SerializerMethodField()
    content_object_url = serializers.SerializerMethodField()

    class Meta:
        model = Category
        fields = [
            'title',
            'slug',
            'subcategories',
            'subcategories_count',
            'content_object_url',
        ]


    def get_subcategories(self, obj):
        return CategoryChildSerializer(obj.children, many=True).data

    def get_subcategories_count(self, obj):
        return obj.children.count()

    def get_content_object_url(self, obj):
        try:
            return obj.get_api_url()
        except:
            None


class CategoryChildSerializer(serializers.ModelSerializer):
    # subcategory_url = category_detail_url
    class Meta:
        model = Category
        fields = [
            'title',
            'slug',
            # 'subcategory_url',
        ]


product_detail_url = HyperlinkedIdentityField(
    view_name='product-api:detail',
    lookup_field='slug'
)


class ProductListSerializer(serializers.ModelSerializer):
    url = product_detail_url
    class Meta:
        model = Item
        fields = [
            'url',
            'item_title',
            'slug',
            'quantity',
            'price',
        ]


class ProductDetailSerializer(serializers.ModelSerializer):
    image = serializers.SerializerMethodField()
    comments = serializers.SerializerMethodField()
    categories = serializers.SerializerMethodField()
    content_object_url = serializers.SerializerMethodField()
    class Meta:
        model = Item
        fields = [
            'item_title',
            'slug',
            'image',
            'description',
            'quantity',
            'price',
            'categories',
            'views',
            'content_object_url',
            'comments',
        ]

    read_only_fields = [
        'views',
        'content_object_url',
    ]

    def get_image(self, obj):
        try:
            image = obj.image.url
        except:
            image = None
        return image

    # def get_comments(self, obj):
        # queryset = Comment.objects.filter(product__slug=obj.slug)
        # comments = CommentDetailSerializer(queryset, many=True).data
        # return comments

    def get_categories(self, obj):
        queryset = Category.objects.filter(items__slug=obj.slug)
        categories = CategoryDetailSerializer(queryset, many=True).data
        return categories

    def get_content_object_url(self, obj):
        try:
            return obj.get_api_url()
        except:
            None
