from django import forms


class ContactForm(forms.Form):
    phone = forms.CharField(max_length=20, required=True)
