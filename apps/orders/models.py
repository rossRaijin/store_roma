from django.db import models
from apps.product.models import Item
from django.utils.translation import gettext_lazy as _
from decimal import Decimal
from apps.coupons.models import Coupon
from django.core.validators import MinValueValidator, MaxValueValidator


class Order(models.Model):
    first_name = models.CharField(max_length=50, verbose_name=_('First name'))
    last_name = models.CharField(max_length=50, verbose_name=_('Last name'))
    email = models.EmailField(verbose_name=_('Email'))
    address = models.CharField(max_length=300, verbose_name=_('Address'))
    postal_code = models.CharField(max_length=20, verbose_name=_('Postal code'))
    city = models.CharField(max_length=100, verbose_name=_('City'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))
    sent = models.BooleanField(default=False, verbose_name=_('Sent'))
    sent_date = models.DateTimeField(auto_now=False,null=True, blank=True, verbose_name=_('Sent date'))
    paid = models.BooleanField(default=False, verbose_name=_('Paid'))
    coupon = models.ForeignKey(Coupon, related_name='orders', null=True, blank=True)
    discount = models.IntegerField(default=0, validators=[MinValueValidator(0), MaxValueValidator(100)])

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return 'Order {}'.format(self.id)

    def is_paid(self):
        if self.paid == True:
            return True
        return False

    def is_sent(self):
        if self.sent == True:
            return True
        return False

    def get_client_full_name(self):
        return '%s %s' % (self.first_name, self.last_name)

    def get_full_address(self):
        return '%s, %s, %s' % (self.postal_code ,self.city, self.address)

    def get_total_cost(self):
        total_cost = sum(item.get_cost() for item in self.items.all())
        return total_cost


class OrderItem(models.Model):
    order = models.ForeignKey(Order, related_name='items', verbose_name=_('Order'))
    product = models.ForeignKey(Item, related_name='order_items', verbose_name=_('Product'))
    price = models.DecimalField(max_digits=10, decimal_places=2, verbose_name=_('Price'))
    quantity = models.PositiveIntegerField(default=1, verbose_name=_('Quantity'))

    def __str__(self):
        return '{}'.format(self.id)

    def get_cost(self):
        return (self.price - self.price*self.product.discount/Decimal('100'))*self.quantity

    def cost_without_discount(self):
        return self.price*self.quantity