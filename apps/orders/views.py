from django.core.mail import send_mail
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.utils import timezone

from apps.product.models import Item
from store_roma import settings
from .models import OrderItem, Order
from .forms import OrderCreateForm
# from .tasks import order_created
from apps.cart.cart import Cart


def order_create(request):
    #Takes the cart from context-processor
    cart = Cart(request)

    if request.method == 'POST':
        form = OrderCreateForm(request.POST)
        # username = request.user.first_name
        if form.is_valid():
            order = form.save(commit=False)
            #Coupons system
            if cart.coupon:
                order.coupon = cart.coupon
                order.discount = cart.coupon.discount
            order.save()
            for item in cart:
                OrderItem.objects.create(order=order,
                                         product=item['item'],
                                         price=item['price'],
                                         quantity=item['quantity'])
                #  Change item.quantity after create order
                buyed = Item.objects.get(id=item['item'].id)
                buyed.quantity -= item['quantity']
                buyed.save()

            #Clear the cart after sending email
            cart.clear()

            #METHOD FOR CELERY, HAS BEEN COMMENTED FOR HOSTING AT PYTHONANYWHERE
            # order_created.delay(order.id)

            # SET THE ORDER IN THE SESSION
            request.session['order_id'] = order.id

            # Send email to buyer
            email = form.cleaned_data['email']
            message = 'Dear {}, \n\nYou have successfully placed an order. Your order id is {}.'.format(request.user,
                                                                                                        order.id)
            subject = 'Order nr. {} issued'.format(order.id)
            from_email = settings.EMAIL_HOST_USER
            to_email = [email, settings.EMAIL_HOST_USER]
            send_mail(subject, message, from_email, to_email, fail_silently=False)

            # REDIRECT TO THE PAYMENT
            return redirect(reverse('payment:process'))
    else:
        if request.user.is_authenticated:
            form = OrderCreateForm(instance=request.user)
        else:
            form = OrderCreateForm()
    return render(request, 'order/create.html', {'form': form})


def order_sent(request, id=None):
    order = get_object_or_404(Order, id=id)
    if order.is_paid():
        order.sent = True
        order.save()
        order.sent_date = timezone.now()
        return redirect(reverse('moderator:panel'))

    email = order.email
    message = 'Dear {}. ' \
              '\n\nYour order {} has been sent to the address indicated by You: {}.' \
              '\n\nYou bought goods for a total of {}.' \
              '\n\nCost of delivery: "be continued)))"'\
        .format(order.get_client_full_name(),
                order.id,
                order.get_full_address(),
                order.get_total_cost(),
                )
    subject = 'Order nr. {} sent. Thank you for using our store'.format(order.id)
    from_email = settings.EMAIL_HOST_USER
    to_email = [email, settings.EMAIL_HOST_USER]
    send_mail(subject, message, from_email, to_email, fail_silently=False)
    return redirect(reverse('moderator:panel'))