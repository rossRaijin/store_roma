from django import forms
from django.contrib.auth.forms import UserCreationForm

from apps.product.models import *
from django.utils.translation import gettext as _


class CreateUserForm(UserCreationForm):

    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'password1',
            'password2',
        ]


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = [
            'title',
            'title_ru',
            'parent',
        ]


class ProductForm(forms.ModelForm):
    class Meta:
        model = Item
        fields = [
            'item_title',
            'description',
            'quantity',
            'price',
            'image',
            'category',
        ]

    def save(self, commit=True):
        instance = super().save(commit)
        if commit:
            for category in self.cleaned_data['category']:
                instance.category.add(category)
        return instance