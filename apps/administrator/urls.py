from django.conf.urls import url
from apps.administrator.views import *
from apps.orders.views import order_sent

urlpatterns = [
    url(r'^panel/', AdminPanel.as_view(), name='panel'),

    url(r'^item_add/$', CreateProduct.as_view(), name='productAdd'),
    url(r'^item_delete/=(?P<pk>\d+)/$', DeleteProduct.as_view(), name='itemDelete'),
    url(r'^item_update/(?P<slug>[\w-]+)/$', UpdateProduct.as_view(), name='itemUpdate'),

    url(r'^cat_add/$', CreateCategory.as_view(), name='categoryAdd'),
    url(r'^cat_update/(?P<slug>[\w-]+)/$', UpdateCategory.as_view(), name='catUpdate'),
    url(r'^cat_delete/=(?P<pk>\d+)/$', DeleteCategory.as_view(), name='catDelete'),

    url(r'^change_staff/(?P<pk>\d+)/$', change_staff, name='staff-change'),
    url(r'^us_create/$', CreateUser.as_view(), name='create-user'),
    url(r'^us_update/(?P<pk>\d+)/$', UpdateUser.as_view(), name='update-user'),
    url(r'^us_delete/=(?P<pk>\d+)/$', DeleteUser.as_view(), name='delete-user'),

    url(r'^paid/(?P<id>\d+)/$', order_is_paid, name='order-paid'),
    url(r'^sent/(?P<id>\d+)/$', order_sent, name='order-sent'),
    url(r'^order_delete/=(?P<id>\d+)/$', DeleteOrder.as_view(), name='delete-order'),
]