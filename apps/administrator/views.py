from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q
from django.http import JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect
from django.views.generic import CreateView, UpdateView, DeleteView, ListView
from apps.administrator.forms import *
from apps.orders.models import Order
from apps.product.models import Item, Category
from django.utils.translation import gettext_lazy as _


class StaffPermissionMixin:
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def post(self, request, *args, **kwargs):
        if request.user.is_superuser or request.user.is_staff:
            return super().post(request, *args, **kwargs)
        else:
            raise Http404


class AdminPermissionMixin:
    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return super().get(request, *args, **kwargs)
        else:
            raise Http404

    def post(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().post(request, *args, **kwargs)
        else:
            raise Http404


class DeletePermissionMixin:
    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_superuser or request.user.is_staff:
            self.object.delete()
            return redirect('moderator:panel')
        else:
            raise Http404


#Admin panel---------------------------------
class AdminPanel(ListView):
    model = Item
    template_name = 'admin/admin_panel.html'
    context_object_name = 'items'
    ordering = ['-updated_date', 'created_date']

    def get_context_data(self, **kwargs):
        context = super(AdminPanel, self).get_context_data(**kwargs)

        if Order.objects.count() != 0:
            # filter for orders
            if 'ord' in self.request.GET:
                context['orders'] = Order.objects.filter(id=self.request.GET.get('ord'))
            else:
                context['orders'] = Order.objects.order_by('created')
        if Category.objects.count() != 0:
            # filter for category
            if 'cat' in self.request.GET:
                context['categories'] = Category.objects.filter(Q(title__startswith=self.request.GET.get('cat')))
            else:
                context['categories'] = Category.objects.all()
        if Item.objects.count() != 0:
            # filter for items(products)
            if 'prod' in self.request.GET:
                context['items'] = Item.objects.filter(Q(item_title__startswith=self.request.GET.get('prod')))
            else:
                context['items'] = Item.objects.all().order_by('-created_date')
        context['users'] = User.objects.all()
        return context


# User administration block---------------------
class CreateUser(AdminPermissionMixin, CreateView):
    model = User
    # permission_denied_message =
    # permission_required = ()
    form_class = CreateUserForm
    template_name = 'admin/user-form-create.html'

    def get_success_url(self):
        return reverse('moderator:panel')


class UpdateUser(AdminPermissionMixin, UpdateView):
    model = User
    form_class = CreateUserForm
    template_name = 'admin/user-form-update.html'

    def get_object(self, queryset=None):
        obj = User.objects.get(pk=self.kwargs['pk'])
        return obj

    def get_success_url(self):
        return reverse('moderator:panel')


class DeleteUser(DeleteView):
    model = User

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_superuser:
            self.object.delete()
            return redirect('moderator:panel')
        else:
            raise Http404


def change_staff(request, pk=None):
    if request.user.is_superuser:
        user = get_object_or_404(User, pk=pk)
        if user.is_staff:
            user.is_staff = False
        else:
            user.is_staff = True
        user.save()
        return redirect(reverse('moderator:panel'))
    else:
        raise Http404


#Category administration block--------------
class CreateCategory(StaffPermissionMixin, CreateView):
    model = Category
    fields = ['title',
              'title_ru',
              'parent',
              'cat_image',
              ]
    template_name = 'admin/category-form-create.html'

    def get_success_url(self):
        return reverse('moderator:panel')


class UpdateCategory(StaffPermissionMixin, UpdateView):
    model = Category
    fields = ['title',
              'title_ru',
              'parent',
              'cat_image',
              ]
    template_name = 'admin/category-form-update.html'
    pk_url_kwarg = 'slug'

    def get_object(self, queryset=None):
        obj = Category.objects.get(slug=self.kwargs['slug'])
        return obj

    def get_success_url(self):
        return reverse('moderator:panel')


class DeleteCategory(DeletePermissionMixin, DeleteView):
    model = Category

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


#Product administration block-------------
class CreateProduct(StaffPermissionMixin, CreateView):
    model = Item
    template_name = 'admin/products-form-create.html'
    fields = ['item_title',
              'item_title_ru',
              'description',
              'quantity',
              'discount',
              'price',
              'image',
              'category',
              ]
    pk_url_kwarg = 'slug'
    slug_field = 'slug'

    def get_success_url(self):
        return reverse('moderator:panel')


class UpdateProduct(StaffPermissionMixin, UpdateView):
    model = Item
    template_name = 'admin/products-form-update.html'
    fields = ['item_title',
              'item_title_ru',
              'description',
              'quantity',
              'discount',
              'price',
              'image',
              'category',
              ]
    slug_field = 'slug'

    def get_success_url(self):
        return reverse('moderator:panel')


class DeleteProduct(DeletePermissionMixin, DeleteView):
    model = Item

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)


#Orders administration block-----------------
def order_is_paid(request, id=None):
    if request.user.is_superuser or request.user.is_staff:
        order = get_object_or_404(Order, id=id)
        if order.paid == True:
            order.paid = False
        else:
            order.paid = True
        order.save()
        return redirect(reverse('moderator:panel'))
    else:
        raise Http404


class DeleteOrder(DeletePermissionMixin, DeleteView):
    model = Order
    pk_url_kwarg = 'id'

    def get(self, request, *args, **kwargs):
        return self.post(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('moderator:panel')
