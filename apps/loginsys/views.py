from django.contrib import auth
from django.shortcuts import redirect, render_to_response, render
from django.template.context_processors import csrf
from apps.loginsys.forms import CreateUserForm
from django.contrib import messages


def login(request):
    args = {}
    args.update(csrf(request))
    if request.POST:
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            messages.success(request, "Log in succcesfully")
            return redirect('product:main_page')
        else:
            messages.error(request, "Login or Password is incorrect")
            return redirect('product:main_page')

    else:
        return redirect('product:main_page')


def logout(request):
    auth.logout(request)

    return redirect('product:main_page')


def register(request):
    args = {}
    args.update(csrf(request))
    args['form'] = CreateUserForm()
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = auth.authenticate(username=username, password=raw_password)
            auth.login(request, user)
            return redirect('product:main_page')
    else:
        args['form'] = CreateUserForm()
    return render(request, 'loginsys/register.html', args)