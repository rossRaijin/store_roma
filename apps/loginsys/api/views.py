from rest_framework import status
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.permissions import IsAdminUser, AllowAny

from apps.loginsys.api.serializers import *
from apps.product.api.pagination import ProductLimitOffsetPagination

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from rest_framework.authtoken.models import Token

from django.contrib.auth.models import User
# User = get_user_model()
from apps.product.api.permissions import IsOwnerOrReadOnly


class UserCreateAPIView(CreateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        user = User.objects.get(username=serializer.instance['username'])
        token, create = Token.objects.get_or_create(user=user)
    # При создании юзера, отображается токен, mail и работает тест
        json = serializer.data
        json['token'] = token.key
        headers = self.get_success_headers(serializer.data)
        return Response(json, status=status.HTTP_201_CREATED, headers=headers)
    # Отображается только юзернейм и не проходит test_create_user_with_take_token
        # headers = self.get_success_headers(serializer.data)
        # return Response({'user': user.username}, status=status.HTTP_201_CREATED, headers=headers)


class UserUpdateAPIView(UpdateAPIView):
    serializer_class = UserCreateSerializer
    queryset = User.objects.all()
    permission_classes = [IsOwnerOrReadOnly]


class UserDeleteAPIView(DestroyAPIView):
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()
    permission_classes = [IsAdminUser]


class UserListAPIView(ListAPIView):
    serializer_class = UserListSerializer
    queryset = User.objects.all()
    pagination_class = ProductLimitOffsetPagination
    permission_classes = [IsAdminUser]


class UserDetailAPIView(RetrieveAPIView):
    serializer_class = UserDetailSerializer
    queryset = User.objects.all()


class UserLoginAPIView(APIView):
    permission_classes = [AllowAny]
    serializer_class = UserLoginSerializer

    def post(self, request, *args, **kwargs):
        data = request.data
        serializer = UserLoginSerializer(data=data)
        if serializer.is_valid(raise_exception=True):
            new_data = serializer.data
            return Response(new_data, status=HTTP_200_OK)
        return Response(serializer.errors, status=HTTP_400_BAD_REQUEST)


class UserLogoutAPIView(APIView):
    queryset = User.objects.all()

    def get(self, request, format=None):
        # simply delete the token to force a login
        request.user.auth_token.delete()
        return Response(status=status.HTTP_200_OK)