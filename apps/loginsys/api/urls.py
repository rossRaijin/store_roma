from django.conf.urls import url

from apps.loginsys.api.views import *

urlpatterns = [
    url(r'^register/$', UserCreateAPIView.as_view(), name="register"),
    url(r'^login/$', UserLoginAPIView.as_view(), name="login"),
    url(r'^logout/$', UserLogoutAPIView.as_view(), name="logout"),
    url(r'^(?P<pk>\d+)/$', UserDetailAPIView.as_view(), name="detail"),
    url(r'^(?P<pk>\d+)/delete/$', UserDeleteAPIView.as_view(), name="delete"),
    url(r'^(?P<pk>\d+)/update/$', UserUpdateAPIView.as_view(), name="update"),
    url(r'^$', UserListAPIView.as_view(), name="list"),
]