import json

from django.contrib.auth.models import User
from django.test import Client
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.authtoken.models import Token

from apps.comments.api.permissions import IsOwnerOrReadOnly, IsAdminOrReadOnly


class AccountsCreateTests(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('testuser',
                                                  'test@gmail.com',
                                                  'testpassword')
        self.create_url = reverse('users-api:register')

    def test_create_user(self):
        data = {
            'username': 'test123user',
            'email': 'test123@gmail.com',
            'password': 'test2password',
            'password2': 'test2password',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)

    # Не будет работать, если во view не выводить token
    def test_create_user_with_take_token(self):
        data = {
            'username': 'test123user',
            'email': 'test123@gmail.com',
            'password': 'test2password',
            'password2': 'test2password',
        }
        response = self.client.post(self.create_url, data, format='json')
        user = User.objects.latest('id')
        token, create = Token.objects.get_or_create(user=user)
        self.assertEqual(response.data['token'], token.key)

    def test_create_user_with_too_long_username(self):
        data = {
            'username': 'foo' * 30,
            'email': 'foobarbaz@example.com',
            'password': 'foobar1234',
            'password2': 'foobar1234',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_create_user_with_no_username(self):
        data = {
            'username': '',
            'email': 'foobarbaz@example.com',
            'password': 'foobar123',
            'password2': 'foobar123'
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_create_user_with_existing_username(self):
        data = {
            'username': 'testuser',
            'email': 'user@example.com',
            'password': 'testpassword',
            'password2': 'testpassword',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['username']), 1)

    def test_create_user_with_short_password(self):
        data = {
            'username': 'foobar',
            'email': 'foobarbaz@example.com',
            'password': 'foo',
            'password2': 'foo',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_no_password(self):
        data = {
            'username': 'foobar',
            'email': 'foobarbaz@example.com',
            'password': '',
            'password2': '',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password']), 1)

    def test_create_user_with_no_input_confirm_password(self):
        data = {
            'username': 'foobar',
            'email': 'foobarbaz@example.com',
            'password': '12345678',
            'password2': '',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['password2']), 1)

    def test_create_user_with_no_correct_confirm_password(self):
        data = {
            'username': 'foobar',
            'email': 'foobarbaz@example.com',
            'password': 'asd123456',
            'password2': 'asd123457',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)

    def test_create_user_with_existing_email(self):
        data = {
            'username': 'testuser2',
            'email': 'test@gmail.com',
            'password': 'testuser123',
            'password2': 'testuser123',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)

    def test_create_user_with_invalid_email(self):
        data = {
            'username': 'foobarbaz',
            'email': 'testing',
            'password': 'testuser123',
            'password2': 'testuser123',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)

    def test_create_user_with_no_email(self):
        data = {
            'username': 'foobar',
            'email': '',
            'password': 'testuser123',
            'password2': 'testuser123',
        }
        response = self.client.post(self.create_url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(User.objects.count(), 1)
        self.assertEqual(len(response.data['email']), 1)


class AccountsLoginTests(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('testuser',
                                                  'test@gmail.com',
                                                  'testpassword',)

    def test_user_login(self):
        response = self.client.login(username=self.test_user.username,
                                     password='testpassword',)
        self.assertTrue(response)

    def test_user_login_with_email(self):
        username = self.test_user.username
        if username is None:
            response = self.client.login(email=self.test_user.email,
                                         password='testpassword',)
            self.assertTrue(response)

    def test_user_login_with_no_exist_or_error_name(self):
        response = self.client.login(username='noexistname',
                                     password='testpassword',)
        self.assertFalse(response)

    def test_user_login_with_no_password(self):
        response = self.client.login(username=self.test_user.username,
                                     password='')
        self.assertFalse(response)

    def test_user_login_with_error_pass(self):
        response = self.client.login(username=self.test_user.username,
                                     password='testpassword1')
        self.assertFalse(response)

    def test_user_get_token_with_login(self):
        token, create = Token.objects.get_or_create(user=self.test_user)
        response = self.client.login(username=self.test_user.username,
                                     password='testpassword', )
        self.assertTrue(response)
        self.assertEqual(token.user, self.test_user)


class AccountsLogoutTests(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('testuser',
                                                  'test@gmail.com',
                                                  'testpassword',)
        token, create = Token.objects.get_or_create(user=self.test_user)

    def test_logout_user(self):
        self.client.logout()
        response = self.test_user.auth_token.delete()
        tokens = Token.objects.all()
        for token in tokens:
            owner = token.user
            self.assertNotEqual(owner, self.test_user)


class AccountTests(APITestCase):

    def setUp(self):
        self.test_user = User.objects.create_user('testuser',
                                                  'test@gmail.com',
                                                  'testpassword',)
        # self.permission = IsAdminOrReadOnly()
        token, create = Token.objects.get_or_create(user=self.test_user)
        self.detail_url = reverse('users-api:detail', kwargs={'pk': self.test_user.pk})
        self.create_url = reverse('users-api:register')

    def test_get_user_detail_page(self):
        self.assertTrue(self.client.login(username='testuser',
                                          password='testpassword'))
        response = self.client.get(self.detail_url,
                                   HTTP_AUTHORIZATION=self.test_user.auth_token,)
        self.assertEqual(response.status_code, 200)
        return_data = json.loads(response.content)
        self.assertTrue('username' in return_data)
        self.assertEqual(return_data['username'], 'testuser')
        # self.assertEqual(return_data['username'], {'username': 'testuser'})

    def test_delete(self):
        self.test_user2 = User.objects.create_user('test2user',
                                                  'test2@gmail.com',
                                                  'test2password')
        self.test_user2.is_staff = True
        self.test_user2.save()
        token, create = Token.objects.get_or_create(user=self.test_user2)
        # self.test_user2.permission = IsAdminOrReadOnly()
        url = '/api/users/2/delete/'
        self.client.login(username=self.test_user2.username,
                          password='test2password',)


        response = self.client.delete(url,
                                      HTTP_AUTHORIZATION=self.test_user2.auth_token,
                                      content_type='application/json',)
        print(response.content)
        print(User.objects.count())
        self.assertEquals(204, response.status_code)


