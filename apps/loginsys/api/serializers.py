from django.contrib.auth import get_user_model
from django.core import exceptions
from django.db.models import Q
from rest_framework.authtoken.models import Token

from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ModelSerializer
from rest_framework import serializers
import django.contrib.auth.password_validation as validators
from rest_framework.relations import HyperlinkedIdentityField

from django.contrib.auth.models import User
# User = get_user_model()
from rest_framework.validators import UniqueValidator


class UserListSerializer(serializers.ModelSerializer):
    url_user_detail = HyperlinkedIdentityField(
        view_name='users-api:detail'
    )
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
            'url_user_detail',
        ]


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'username',
            'first_name',
            'last_name',
            'email',
        ]

class UserCreateSerializer(ModelSerializer):
    username = serializers.CharField(max_length=40,
                                     validators=[UniqueValidator(queryset=User.objects.all())])
    email = serializers.EmailField(required=True,
                                   validators=[UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(label='Password', write_only=True)
    password2 = serializers.CharField(label='Confirm Password', write_only=True)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'password2',
        ]

    def validate(self, data):
        data = self.get_initial()
        password = data.get("password")
        errors = dict()
        try:
            # validate the password and catch the exception
            validators.validate_password(password=password, user=User)
        # the exception raised here is different than serializers.ValidationError
        except exceptions.ValidationError as e:
            errors['password'] = list(e.messages)
        if errors:
            raise serializers.ValidationError(errors)

        password2 = data.get("password2")
        if password != password2:
            raise ValidationError("ppc")
        email = data.get('email')
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise ValidationError('Takoi email est')
        return data

    def create(self, validated_data):
        username = validated_data['username']
        email = validated_data['email']
        password = validated_data['password']
        user_obj = User(username=username, email = email)
        user_obj.set_password(password)
        user_obj.save()
        return validated_data


class UserLoginSerializer(ModelSerializer):
    token = serializers.BooleanField(default=False, read_only=True)
    username = serializers.CharField(required=False, allow_blank=True)
    password = serializers.CharField(allow_blank=False, write_only=True)
    email = serializers.EmailField(label='Email', required=False, allow_blank=True)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
            'token',
        ]

    def validate(self, data):
        user_obj = None
        username = data.get('username', None)
        email = data.get('email', None)
        password = data['password']
        if not username and not email:
            raise ValidationError('Username or email is required to login.')
        user = User.objects.filter(Q(email=email) | Q(username=username)).distinct()
        if user.exists() and user.count() == 1:
            user_obj = user.first()
        else:
            raise ValidationError('This username/email is not valid.')
        if user_obj:
            if not user_obj.check_password(password):
                raise ValidationError('Password is incorrect. Try again.')
            if not username:
                username = user_obj.username
            else:
                email = user_obj.email
        data['username'] = username
        data['email'] = email
        try:
            token = Token.objects.get(user=user)
        except:
            token = Token.objects.create(user=user_obj)
        data['token'] = token
        return data