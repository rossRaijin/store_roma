import telepot as telepot

from django.shortcuts import redirect
from apps.product.forms import ContactForm


token = '469723110:AAEewGk34x63M7tzKYiIQhohvx86ykR6tfU'
my_id = 521853533
telegramBot = telepot.Bot(token)


def handle(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            phone = form.cleaned_data['phone']
            message = "*ЗАЯВКА С САЙТА*:" + "\n" + "*ТЕЛЕФОН*: " + str(phone)
            telegramBot.sendMessage(521853533, message, parse_mode="Markdown")
            return redirect('product:main_page')
        return redirect('product:main_page')