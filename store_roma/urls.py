from django.conf.urls import url, include
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls.i18n import i18n_patterns

from store_roma import telegramm
from store_roma import search

admin.autodiscover()


urlpatterns = i18n_patterns(
    url(r'^admin/', admin.site.urls),
    url(r'^search/$', search.GlobalSearch.as_view(), name="site-search"),
    url(r'^feedback/$', telegramm.handle, name='feedback'),
    url(r'^account/', include('apps.account.urls', namespace="account")),
    url(r'^cart/', include('apps.cart.urls', namespace="cart")),
    url(r'^orders/', include('apps.orders.urls', namespace="orders")),
    # url(r'^comment/', include('apps.comments.urls', namespace="comment")),
    url(r'^wall/', include('apps.wall.urls', namespace='wall')),
    url(r'^moderator/', include('apps.administrator.urls', namespace="moderator")),
    url(r'^auth/', include('apps.loginsys.urls', namespace="auth")),
    url(r'^coupons/', include('apps.coupons.urls', namespace="coupons")),
    url(r'^api/products/', include('apps.product.api.urls', namespace='product-api')),
    # url(r'^api/comments/', include('apps.comments.api.urls', namespace='comment-api')),
    url(r'^api/users/', include('apps.loginsys.api.urls', namespace='users-api')),
    url(r'^paypal/', include('paypal.standard.ipn.urls')),
    url(r'^payment/', include('apps.payment.urls', namespace='payment')),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^', include('apps.product.urls', namespace="product")),
    prefix_default_language=False,
)

urlpatterns += staticfiles_urlpatterns()

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
