from django.views.generic import ListView
from django_filters.views import FilterView
from django.utils.translation import gettext as _

from apps.product.filters import ProductFilter
from apps.product.models import Item
from apps.product.views import MyMixin


class GlobalSearch(FilterView, MyMixin, ListView):
    """
    Returned page with list of items,
    according query from search-field
    in top-section
    """
    model = Item
    template_name = 'product/product-list.html'
    #  items list can additionally filtered
    filterset_class = ProductFilter
    context_object_name = 'items'
    paginate_by = 1
    page_kwarg = _('page')

    def get_queryset(self):
        my_list = Item.objects.all()
        query = self.request.GET.get('bigs')
        if query:
            my_list = my_list.filter(item_title__icontains=query)
        return my_list
