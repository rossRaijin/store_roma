
class AppendTextMiddleWare(object):
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        print("before the view is executed")
        response = self.get_response(request)
        print("after the view is executed")
        return response
